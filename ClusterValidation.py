import math

class ClusterValidation:
    def __init__(self):
        self.cluster = {}
        self.truth = {}
        self.mutual = {}
        self.points = 0
        self.data_points = []
        self.nmi = 0.0
        self.fc = 0.0
        self.jc = 0.0
        self.tp = 0
        self.fp = 0
        self.fn = 0

    def insert_cluster(self, key):
        if key not in self.cluster:
            self.cluster[key] = 1
        else:
            self.cluster[key] = int(self.cluster[key]) + 1

    def insert_truth(self, key):
        if key not in self.truth:
            self.truth[key] = 1
        else:
            self.truth[key] = int(self.truth[key]) + 1

    def insert_mutual(self, key):
        if key not in self.mutual:
            self.mutual[key] = 1
        else:
            self.mutual[key] = int(self.mutual[key]) + 1

    def inc_tp(self):
        self.tp += 1

    def inc_fp(self):
        self.fp += 1

    def inc_fn(self):
        self.fn += 1

    def set_points(self, points):
        self.points = points

    def output(self, key, value):
        with open("patterns.txt", 'a') as file:
            line = str(value) + ":"
            words = key.rstrip().split(' ')
            for word in words:
                line += word + ";"
            line = line[:-1]
            line += "\n"
            file.write(line)

    def compute_entropy(self):
        ht = 0.0
        hc = 0.0
        ic = 0.0
        for key, value in self.truth.items():
            ht += float(value)/self.points * (math.log(float(value)/self.points,2))
        ht = -ht
        for key,value in self.cluster.items():
            hc += float(value) / self.points * (math.log(float(value) / self.points, 2))
        hc = -hc
        for key,value in self.mutual.items():
            p = float(value)/self.points
            t = float(self.truth[key[0]])/self.points
            c = float(self.cluster[key[1]])/self.points
            ic += p * math.log((p/(t*c)),2)
        self.nmi = round((ic / math.sqrt(ht*hc)),3)


    def compute_jc(self):
        TP = 0
        FN = 0
        FP = 0
        for i in range(0,len(self.data_points)):
            for j in range(i+1, len(self.data_points)):
                point_i = self.data_points[i]
                point_j = self.data_points[j]
                if point_i[0] == point_j[0] and point_i[1] == point_j[1]:
                    TP += 1
                if point_i[0] == point_j[0] and point_i[1] != point_j[1]:
                    FN += 1
                if point_i[0] != point_j[0] and point_i[1] == point_j[1]:
                    FP += 1
        if TP == 0:
            self.jc = format(0.0, '.3f')
        else:
            num = float(TP)
            denum = float(TP + FN + FP)
            self.jc = round((float(num) / denum),3)

if __name__ == '__main__':

    validation = ClusterValidation()

    # Accept input and store locally
    file = open("input0.txt", "r")
    lines = file.readlines()

    validation.set_points(len(lines))

    # Read file line by line and create the 3 dictionaries for H(C), H(T) and I(C,T)
    for line in lines:
        data = line.rstrip().split(' ')
        if(data[0] == data[1]):
            validation.inc_tp()

        validation.insert_truth(data[0])
        validation.insert_cluster(data[1])
        mutual_array = (data[0], data[1])
        validation.insert_mutual(mutual_array)
        validation.data_points.append([data[0],data[1]])

    # Convert the data to probabilities
    validation.compute_entropy()

    validation.compute_jc()

    print validation.nmi, validation.jc


