This code implements two clustering validation measures: Normalized Mutual Information (NMI) and Jaccard similarity.


Additional Test cases are added as follows:
The input of each test case includes n lines. 
Each line i includes two numbers separated by a space. 
The first number is the ground-truth cluster label of the i-th instance, 
and the second number is the predicted cluster label of the i-th instance. 

The algorithm evaluates the clustering predictions with regard to the ground-truth by NMI and Jaccard measures. 